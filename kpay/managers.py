from django.db import models


class BaseQuerySet(models.QuerySet):

    def active(self):
        return self.filter(is_active=True)


class BaseManager(models.Manager):

    def get_queryset(self):
        return BaseQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().filter(is_active=True)
