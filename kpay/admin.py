from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from kpay.models import Gateway, Payment
from kpay.attrs import GatewayAttr, PaymentAttr


@admin.register(Gateway)
class GatewayAdmin(admin.ModelAdmin):
    list_display = GatewayAttr.admin_list_display
    list_filter = GatewayAttr.admin_list_filter
    list_editable = GatewayAttr.admin_list_editable
    search_fields = GatewayAttr.admin_search_fields


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = PaymentAttr.admin_list_display
    list_filter = PaymentAttr.admin_list_filter
    search_fields = PaymentAttr.admin_search_fields

    def check_status(self, obj):
        url = reverse('kpay:status', args=(obj.pk,))
        return mark_safe(f'<a href="{url}" target="_blank">Check</a>')
