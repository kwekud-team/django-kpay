from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand, CommandError
from kpay.contrib.utils.gateway import populate_gateways


class Command(BaseCommand):
    help = 'Populate gateways'

    def add_arguments(self, parser):
        parser.add_argument(
            '--site_id',
            action='store_true',
            help='Site id',
        )

    def handle(self, *args, **options):
        site_id = options['site_id']
        site = Site.objects.filter(pk=site_id).first()
        if not site:
            site = Site.objects.get_current()

        if site:
            populate_gateways(site)
        else:
            raise CommandError('Site "%s" does not exist' % site_id)
        self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % site_id))
