import json
import logging
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404

from kpay.constants import KpayK
from kpay.contrib.utils.pay import PayUtils
from kpay.models import Payment


class PayCallbackView(TemplateView):
    template_name = "kpay/callback.html"

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(PayCallbackView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'payment_status': self.get_payment_status()
        })
        return context

    def get_payment_status(self):
        return PayUtils(self.request).get_payment_status(
            KpayK.FUNC_SOURCE_CALLBACK.value,
            self.kwargs['gateway'],
            json.dumps(dict(self.request.GET.items()))
        )


class PayWebhookView(TemplateView):
    gateway = ''

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        self.gateway = kwargs['gateway']
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self._process_request()

    def post(self, request, *args, **kwargs):
        logging.debug(f'Pay end: Gateway: {self.gateway}, ct: {self.request.content_type}, body: {self.request.body}')
        return self._process_request()

    def _process_request(self):
        payment_process = PayUtils(self.request).process_payment_by_webhook(self.gateway, self.request.body)
        if payment_process.is_valid:
            return HttpResponse('Success')
        else:
            return HttpResponse('Error', status=400)


class PayStatusView(TemplateView):
    payment = None

    def dispatch(self, request, *args, **kwargs):
        self.payment = get_object_or_404(Payment, pk=kwargs['payment_pk'])
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # TODO: Use post method instead
        payment_process = PayUtils(self.request).process_payment_by_status(self.payment)
        if payment_process.is_valid:
            return HttpResponse('Success')
        else:
            return HttpResponse('Error', status=400)
