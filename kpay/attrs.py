

class GatewayAttr:
    admin_list_display = ['identifier', 'name', 'description', 'sort', 'is_active', 'site']
    admin_list_filter = ['site', 'is_active']
    admin_search_fields = ['name']
    admin_list_editable = ['sort', 'is_active']
    admin_form_fields = ['site', 'gateway', 'name', 'description', 'sort', 'image', 'is_active']


class PaymentAttr:
    admin_list_display = ['pk', 'gateway', 'status', 'remote_ref', 'source_app_model', 'source_ref', 'currency',
                          'total_amount', 'is_valid', 'captured_amount', 'date_completed', 'date_validated',
                          'local_ref', 'method', 'provider', 'provider_account', 'provider_ref', 'method', 'site',
                          'check_status', 'simulate_type']
    admin_list_filter = ['site', 'is_active', 'status', 'gateway', 'currency', 'simulate_type']
    admin_search_fields = ['remote_ref', 'source_ref', 'local_ref']
