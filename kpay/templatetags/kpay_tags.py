from django import template
from kpay.models import Gateway

register = template.Library()


@register.simple_tag()
def get_gateway_list():
    return Gateway.objects.filter(is_active=True)
