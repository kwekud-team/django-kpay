import json
from django.utils import timezone
from kommons.utils.generic import decify, get_dotted_dict
from kpay.backends.base import BaseBackend
from kpay.constants import KpayK
from kpay.contrib.dclasses import PaymentLink, PaymentProcess, PaymentStatus


class FreeBackend(BaseBackend):
    identifier = KpayK.BACKEND_FREE.value
    name = 'Free'
    description = 'Default payment gateway for development purposes only.'

    def get_payment_link(self, payment) -> PaymentLink:
        return PaymentLink(
            is_valid=True,
            code='00',
            message='Success',
            payment=payment,
            payment_local_ref=payment.local_ref,
            response={}
        )

    def process_payment(self, remote_data) -> PaymentProcess:
        remote_json = json.loads(remote_data)
        now = timezone.now()

        if remote_json.get('code') == "0":
            payment = self.get_payment_by_ref(
                get_dotted_dict(remote_json, 'client_ref'),
                get_dotted_dict(remote_json, 'remote_ref')
            )

            captured_amount = decify(get_dotted_dict(remote_json, 'captured_amount'))
            status = get_dotted_dict(remote_json, 'status', '')

            date_completed = now
            date_validated = now if status == 'Success' else None

            return PaymentProcess(
                is_valid=True,
                message='Payment successful',
                payment=payment,
                date_completed=date_completed,
                date_validated=date_validated,
                captured_amount=captured_amount,
                payment_provider='',
                response=remote_json
            )
        elif remote_json.get('ResponseCode') == "-1":
            #  TODO: Handle different response codes. Eg: Refund
            return PaymentProcess(
                is_valid=False,
                message='Response not handled',
            )
        else:
            return PaymentProcess(is_valid=False, message='Payment not processed')

    def get_payment_status(self, source, remote_data) -> PaymentStatus:
        raise NotImplementedError
