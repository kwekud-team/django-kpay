import logging
import requests
from django.utils import timezone
from kommons.utils.generic import decify, jsonify, floatify, get_dotted_dict
from kommons.utils.dates import parse_date
from kpay.constants import KpayK
from kpay.backends.base import BaseBackend
from kpay.contrib.dclasses import PaymentLink, PaymentProcess, PaymentStatus
from kpay.models import Payment


class HubtelBusinessBackend(BaseBackend):
    identifier = KpayK.BACKEND_HUBTEL_BUSINESS.value
    name = 'Hubtel: Business'
    description = 'Hubtel business payments'
    paylink_url = 'https://payproxyapi.hubtel.com/items/initiate'
    status_url = 'https://api-txnstatus.hubtel.com/transactions/status'

    def get_paylink_url(self):
        return self.get_setting('paylink_url') or self.paylink_url

    def get_status_url(self):
        return self.get_setting('status_url') or self.status_url

    def get_payment_link(self, payment) -> PaymentLink:
        if not payment.date_completed:
            resp_json = get_dotted_dict(payment.response, 'start', {})
            if resp_json:
                data = get_dotted_dict(resp_json, 'data', {})
                # TODO: write tests for when data is not expected format
                if isinstance(data, dict):
                    return PaymentLink(
                        is_valid=True,
                        is_new=False,
                        code=get_dotted_dict(resp_json, 'status', ''),
                        message=get_dotted_dict(data, 'message', ''),
                        payment=payment,
                        payment_local_ref=get_dotted_dict(data, 'clientReference', ''),
                        payment_remote_ref=get_dotted_dict(data, 'checkoutId', ''),
                        payment_remote_url=get_dotted_dict(data, 'checkoutDirectUrl', ''),
                        payment_callback_url=get_dotted_dict(resp_json, 'urls.callback_url', ''),
                        payment_redirect_url=get_dotted_dict(resp_json, 'urls.redirect_url', ''),
                        response=resp_json
                    )

        merchant_number = self.get_setting('merchant_number')
        username = self.get_setting('username')
        password = self.get_setting('password')
        cancel_url = self.get_setting('cancel_url')
        redirect_url = self.get_redirect_url()
        webhook_url = self.get_webhook_url()

        payload = {
            "totalAmount": float(payment.total_amount),
            "description": payment.description,
            "callbackUrl": webhook_url,
            "returnUrl": redirect_url,
            "merchantAccountNumber": merchant_number,
            "cancellationUrl": cancel_url,
            "clientReference": payment.local_ref
        }

        resp = requests.post(self.get_paylink_url(), json=payload, auth=(username, password))
        try:
            resp_json = resp.json()
        except Exception as e:
            logging.error(f'{payment.gateway.identifier}: Cannot get json from response. {e}')
            resp_json = {}

        if resp.status_code == 200 and str(get_dotted_dict(resp_json, 'responseCode')) == '0000':
            data = get_dotted_dict(resp_json, 'data', {})
            return PaymentLink(
                is_valid=True,
                code=get_dotted_dict(resp_json, 'status', ''),
                message=get_dotted_dict(data, 'message', ''),
                payment=payment,
                payment_local_ref=get_dotted_dict(data, 'clientReference', ''),
                payment_remote_ref=get_dotted_dict(data, 'checkoutId', ''),
                payment_remote_url=get_dotted_dict(data, 'checkoutDirectUrl', ''),
                payment_callback_url=webhook_url,
                payment_redirect_url=redirect_url,
                response=resp_json
            )

        data = get_dotted_dict(resp_json, 'data', {})
        if data and isinstance(data, list):
            message = get_dotted_dict(data[0], 'errorMessage', '')
        else:
            message = get_dotted_dict(resp_json, 'data.errorMessage', '')

        return PaymentLink(
            is_valid=False,
            code=get_dotted_dict(resp_json, 'status', ''),
            message=message,
            payment=payment,
            payment_local_ref=payment.local_ref,
            response=resp_json
        )

    def process_payment_by_webhook(self, remote_data) -> PaymentProcess:
        now = timezone.now()
        remote_json = jsonify(remote_data)

        resp_code = get_dotted_dict(remote_json, 'ResponseCode')
        data = get_dotted_dict(remote_json, 'data', {})
        payment = self.get_payment_by_ref(
            get_dotted_dict(data, 'clientReference'),
            get_dotted_dict(data, 'checkoutId')
        )

        captured_amount = decify(get_dotted_dict(data, 'Amount'))
        method = get_dotted_dict(data, 'PaymentDetails.PaymentType', '')
        provider = get_dotted_dict(data, 'PaymentDetails.Channel', '')
        provider_account = get_dotted_dict(data, 'PaymentDetails.MobileMoneyNumber', '')  # TODO: Test bank card
        description = get_dotted_dict(remote_json, 'description')

        is_valid = resp_code == '0000'
        message = KpayK.PAY_MESSAGE_SUCCESS.value if is_valid else KpayK.PAY_MESSAGE_FAILURE.value
        date_completed = now
        date_validated = now if is_valid else None

        return PaymentProcess(
            process_function=KpayK.EPF_WEBHOOK.value,
            is_valid=is_valid,
            message=message,
            description=description,
            payment=payment,
            date_completed=date_completed,
            date_validated=date_validated,
            captured_amount=captured_amount,
            payment_method=method,
            payment_provider_account=provider_account,
            payment_provider=provider,
            response=remote_json,
            update_fields=['captured_amount', 'response', 'date_validated_webhook', 'provider', 'method',
                           'provider_account', 'description']
        )

    def process_payment_by_status(self, payment) -> PaymentProcess:
        now = timezone.now()
        auth_token = self.get_setting('auth_token')

        if payment.simulate_type:
            resp_json = self._get_simulated_json(payment)
            is_valid_resp = True
        else:
            headers = {'Authorization': f'Basic {auth_token}'}
            payload = {"Clientreference": payment.local_ref}
            resp = requests.get(self.get_status_url(), params=payload, headers=headers)
            try:
                resp_json = resp.json()
            except Exception as e:
                logging.error(f'{payment.gateway.identifier}: Cannot get json from response. {e}')
                resp_json = {}

            resp_code = str(get_dotted_dict(resp_json, 'responseCode'))
            is_valid_resp = resp.status_code == 200  # and resp_code == '0000'

        status = str(get_dotted_dict(resp_json, 'data.status'))
        if is_valid_resp:
            is_valid = status.lower() == 'paid'
            message = f'{KpayK.PAY_MESSAGE_SUCCESS.value if is_valid else KpayK.PAY_MESSAGE_FAILURE.value}'
            date_completed = now
            date_validated = now if is_valid else None
            date_paid = None

            captured_amount = decify(get_dotted_dict(resp_json, 'data.Amount'))
            method = get_dotted_dict(resp_json, 'data.paymentMethod', '')
            provider_account = get_dotted_dict(resp_json, 'data.paymentMethod', '')
            provider_ref = get_dotted_dict(resp_json, 'data.externalTransactionId', '')
            description = get_dotted_dict(resp_json, 'data.status')
            date_paid_str = get_dotted_dict(resp_json, 'data.date')

            update_fields = ['captured_amount', 'response', 'date_validated_status', 'method', 'provider_ref']
            if not payment.provider_account:
                # Hubtel does not provide provider account in status response, so we use paymentMethod instead
                update_fields.append('provider_account')

            if not payment.description:
                # Description in webhook is better
                update_fields.append('description')

            if is_valid:
                date_paid = parse_date(date_paid_str)
                update_fields.append('date_paid')

            return PaymentProcess(
                process_function=KpayK.EPF_STATUS.value,
                is_valid=is_valid,
                message=message,
                description=description,
                payment=payment,
                date_completed=date_completed,
                date_validated=date_validated,
                date_paid=date_paid,
                captured_amount=captured_amount,
                payment_method=method,
                payment_provider_account=provider_account,
                payment_provider_ref=provider_ref,
                response=resp_json,
                update_fields=update_fields
            )

        return PaymentProcess(process_function=KpayK.EPF_STATUS.value, is_valid=False, payment=payment,
                              message='Payment response not valid')

    def get_payment_status(self, source, remote_data) -> PaymentStatus:
        remote_json = jsonify(remote_data)
        payment = None
        checkout_id = get_dotted_dict(remote_json, 'checkoutId')
        if checkout_id:
            payment = Payment.objects.filter(remote_ref=checkout_id).first()
            self.commit_status_payment(payment)

        if payment:
            return self.get_local_payment_status(payment)
        else:
            return PaymentStatus(is_valid=False, message='Payment not found', total_amount=decify(0))

    def _get_simulated_json(self, payment):
        if payment.simulate_type == KpayK.STATUS_FAILURE:
            return {
              "message": "Successful",
              "responseCode": "0000",
              "data": {
                "date": "2022-10-10T19:08:30.3460725Z",
                "status": "Unpaid",
                "transactionId": "e553e129a63f4117b956c9b2b34dfd89",
                "externalTransactionId": None,
                "paymentMethod": None,
                "clientReference": payment.local_ref,
                "currencyCode": None,
                "amount": floatify(payment.total_amount),
                "charges": None,
                "amountAfterCharges": None,
                "isFulfilled": None
              }
            }
        else:
            return {
              "message": "Successful",
              "responseCode": "0000",
              "data": {
                "date": "2022-09-07T12:22:39.7666571Z",
                "status": "Paid",
                "transactionId": "139c2fa190e444a0b6e41414529009a6",
                "externalTransactionId": timezone.now().strftime('%Y%m%d%H%M%S'),
                "paymentMethod": "mobilemoney",
                "clientReference": payment.local_ref,
                "currencyCode": None,
                "amount": floatify(payment.total_amount),
                "charges": 0.01,
                "amountAfterCharges": 0.1,
                "isFulfilled": None
              }
            }


class HubtelPersonalBackend(BaseBackend):
    identifier = KpayK.BACKEND_HUBTEL_PERSONAL.value
    name = 'Hubtel: Personal'
    description = 'Hubtel personal payments'

    def get_payment_link(self, payment) -> PaymentLink:
        # To be implemented
        pass

    def process_payment(self, remote_data) -> PaymentProcess:
        # To be implemented
        pass

    def get_payment_status(self, source, remote_data) -> PaymentProcess:
        # To be implemented
        pass
