
# def webhook_event_callback(event):
#     from djstripe.models import WebhookEventTrigger
#     from stripe.error import StripeError
#
#     pk = event.pk
#     try:
#         # get the event
#         obj = WebhookEventTrigger.objects.get(pk=pk)
#         # process the event.
#         # internally, this creates a Stripe WebhookEvent Object and invokes the respective Webhooks
#         event = obj.process()
#     except StripeError as exc:
#         pass
#     except WebhookEventTrigger.DoesNotExist as exc:
#         # This can happen in case the celery task got executed before the actual model got saved to the DB
#         pass
#
#     return event.type or "Stripe Event Processed"