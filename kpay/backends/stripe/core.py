import stripe
from django.urls import reverse
from django.conf import settings
from kpay.backends.base import BaseBackend
from kpay.constants import KpayK


class StripeBackend(BaseBackend):
    identifier = KpayK.BACKEND_STRIPE.value
    name = 'Stripe'
    description = 'Offers payment processing software and application programming interfaces for e-commerce websites ' \
                  'and mobile applications.'

    def get_payment_url(self, payment):
        stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

        results_url = reverse('kpay:payment_results', args=(payment.pk,))
        full_url = self.request.build_absolute_uri(results_url)

        session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            idempotency_key=str(payment.guid),
            metadata={'payment_ref': str(payment.guid)},
            line_items=[{
                'price': 'price_1JQpvkAYwITb5kHFWOgpzMaU',
                'quantity': 1,
            }],
            mode='subscription',
            success_url=f'{full_url}?type=success&session_id=' + '{CHECKOUT_SESSION_ID}',
            cancel_url=f'{full_url}?type=cancel',
        )
        return session.url
