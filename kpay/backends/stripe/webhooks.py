from django.db import transaction
from django.utils import timezone
# from djstripe import webhooks
from kommons.utils.generic import get_dotted_dict

from kpay.contrib.signals.dispatch import kpay_status_changed
from kpay.models import Payment


def do_something():
    pass


# @webhooks.handler("charge.succeeded")
def my_handler(event, **kwargs):
    paid = get_dotted_dict(event.data, 'object.paid')
    transaction_id = get_dotted_dict(event.data, 'object.id')
    amount_captured = get_dotted_dict(event.data, 'object.amount_captured')
    status = get_dotted_dict(event.data, 'object.status')
    # print(paid, transaction_id, amount_captured, status, event.idempotency_key, event.metadata)

    if event.idempotency_key:
        payment = Payment.objects.filter(guid=event.idempotency_key).first()
        if payment:
            date_completed = timezone.now()
            values = {
                'captured_amount': amount_captured,
                'transaction_id': transaction_id,
                'status': status,
            }
            if status == 'succeeded':
                values.update({
                    'captured_amount': amount_captured,
                    'transaction_id': transaction_id,
                    'status': status,
                    'date_completed': date_completed,
                    'date_validated': date_completed
                })
            elif status == 'failed':
                values.update({
                    'date_completed': date_completed,
                })

            Payment.objects.filter(pk=payment.pk).update(**values)
            kpay_status_changed.send(sender=Payment, instance=payment)

    print("AAAAAAAAAA", event, payment, event.data['object']['paid'])
    cs
    transaction.on_commit(do_something)
