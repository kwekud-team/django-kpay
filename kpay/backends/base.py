import decimal
from django.urls import reverse

from kpay.constants import KpayK
from kpay.models import Payment
from kpay.contrib.dclasses import PaymentLink, PaymentProcess, PaymentStatus


class BaseBackend:
    name = ''
    description = ''
    end_payment_func = KpayK.EPF_STATUS.value

    def __init__(self, request, gateway, **kwargs):
        self.request = request
        self.gateway = gateway

    def get_payment_link(self, payment) -> PaymentLink:
        raise NotImplementedError

    def process_payment_by_webhook(self, remote_data) -> PaymentProcess:
        raise NotImplementedError

    def process_payment_by_status(self, payment) -> PaymentProcess:
        raise NotImplementedError

    def get_payment_status(self, source, remote_data) -> PaymentStatus:
        raise NotImplementedError

    def get_setting(self, key, default=None):
        return self.gateway.settings.get(key, default)

    def get_redirect_url(self):
        url = reverse('kpay:return', args=(self.gateway.identifier,))
        return f'{self.gateway.site.domain}{url}'

    def get_webhook_url(self):
        url = reverse('kpay:webhook', args=(self.gateway.identifier,))
        return f'{self.gateway.site.domain}{url}'

    def get_local_payment_status(self, payment):
        if not payment:
            return PaymentStatus(is_valid=False, message=KpayK.PAY_MESSAGE_MISSING.value,
                                 total_amount=decimal.Decimal(0))

        is_valid = payment.date_validated is not None
        if payment.date_completed:
            message = KpayK.PAY_MESSAGE_SUCCESS.value if is_valid else KpayK.PAY_MESSAGE_FAILURE.value
        else:
            message = KpayK.PAY_MESSAGE_PENDING.value

        return PaymentStatus(
            is_valid=is_valid,
            message=message,
            payment=payment,
            date_completed=payment.date_completed,
            date_validated=payment.date_validated,
            total_amount=payment.total_amount,
            captured_amount=payment.captured_amount,
            payment_provider=payment.provider,
        )

    def get_payment_by_ref(self, local_ref, remote_ref):
        # 1. Get payment by local ref
        payment = Payment.objects.filter(local_ref=local_ref).first()

        # 2. Get payment by remote ref, it's possible to have multiple remote_ref
        if not payment:
            payment = Payment.objects.filter(remote_ref=remote_ref).order_by('-date_created',).first()

        # TODO: 3. Zombie payment. Create a local payment record and investigate manually later
        return payment

    def commit_webhook_payment(self, gateway, remote_data):
        simulate_type = self.request.GET.get('st', '')

        payment_process = self.process_payment_by_webhook(remote_data)
        if payment_process.payment:
            update_fields = payment_process.update_fields
            payment = payment_process.payment
            payment.date_validated_webhook = payment_process.date_validated
            payment.captured_amount = payment_process.captured_amount
            payment.method = payment_process.payment_method
            payment.provider_account = payment_process.payment_provider_account
            payment.provider = payment_process.payment_provider
            payment.provider_ref = payment_process.payment_provider_ref
            payment.response.update({'webhook': payment_process.response or {}})
            payment.description = payment_process.description
            payment.simulate_type = simulate_type

            update_fields.append('simulate_type')

            if self.end_payment_func == KpayK.EPF_WEBHOOK.value:
                payment.is_valid = payment_process.is_valid
                payment.date_completed = payment_process.date_completed
                payment.date_validated = payment_process.date_validated
                if payment_process.date_completed:
                    payment.status = KpayK.STATUS_PAYMENT_END.value
                else:
                    payment.status = KpayK.STATUS_PAYMENT_PENDING.value
                update_fields.extend(['is_valid', 'date_completed', 'date_validated', 'status'])

            payment.save(update_fields=update_fields)
            return payment_process

        return PaymentProcess(process_function=KpayK.EPF_STATUS.value, is_valid=False, message='Payment not found')

    def commit_status_payment(self, payment):
        payment_process = self.process_payment_by_status(payment)

        update_fields = payment_process.update_fields
        payment = payment_process.payment
        payment.date_validated_status = payment_process.date_validated
        payment.captured_amount = payment_process.captured_amount
        payment.method = payment_process.payment_method
        payment.provider = payment_process.payment_provider
        payment.provider_account = payment_process.payment_provider_account
        payment.provider_ref = payment_process.payment_provider_ref
        payment.description = payment_process.description
        payment.date_paid = payment_process.date_paid
        payment.response.update({'status': payment_process.response or {}})

        if self.end_payment_func == KpayK.EPF_STATUS.value:
            payment.is_valid = payment_process.is_valid
            payment.date_completed = payment_process.date_completed
            payment.date_validated = payment_process.date_validated
            if payment_process.date_completed:
                payment.status = KpayK.STATUS_PAYMENT_END.value
            else:
                payment.status = KpayK.STATUS_PAYMENT_PENDING.value
            update_fields.extend(['is_valid', 'date_completed', 'date_validated', 'status'])

        payment.save(update_fields=update_fields)
        return payment_process
