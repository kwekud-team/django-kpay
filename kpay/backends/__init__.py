from kpay.constants import KpayK
from kpay.backends.free import FreeBackend
from kpay.backends.stripe.core import StripeBackend
from kpay.backends.hubtel.core import HubtelBusinessBackend, HubtelPersonalBackend


REGISTRY = {
    KpayK.BACKEND_FREE.value: FreeBackend,
    KpayK.BACKEND_STRIPE.value: StripeBackend,
    KpayK.BACKEND_HUBTEL_BUSINESS.value: HubtelBusinessBackend,
    KpayK.BACKEND_HUBTEL_PERSONAL.value: HubtelPersonalBackend,
}
