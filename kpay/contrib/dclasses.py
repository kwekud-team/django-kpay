import datetime
from dataclasses import dataclass, field
from decimal import Decimal
from typing import List

from kpay.models import Payment


@dataclass
class PaymentItem:
    source_app_model: str
    source_ref: str
    description: str
    currency: str
    amount: Decimal


@dataclass
class PaymentLink:
    is_valid: bool
    code: str
    message: str
    payment: Payment
    payment_local_ref: str
    payment_remote_ref: str = ''
    payment_remote_url: str = ''
    payment_redirect_url: str = ''
    payment_callback_url: str = ''
    is_new: bool = True
    response: dict = field(default_factory=lambda: {})


@dataclass
class PaymentProcess:
    process_function: str
    is_valid: bool
    message: str
    payment: Payment = None
    date_completed: datetime.datetime = None
    date_validated: datetime.datetime = None
    date_paid: datetime.datetime = None
    captured_amount: Decimal = Decimal(0)
    description: str = ''
    payment_method: str = ''
    payment_provider: str = ''
    payment_provider_account: str = ''
    payment_provider_ref: str = ''
    response: dict = field(default_factory=lambda: {})
    update_fields: List[str] = field(default_factory=list)

    def as_json_dict(self):
        return {
            'is_valid': self.is_valid,
            'message': self.message,
            'payment': self.payment.local_ref if self.payment else None,
            'date_completed': str(self.date_completed) if self.date_completed else None,
            'date_validated': str(self.date_validated) if self.date_validated else None,
            'captured_amount': str(self.captured_amount),
            'payment_provider': self.payment_provider or '',
            'response': self.response or {}
        }


@dataclass
class PaymentStatus:
    is_valid: bool
    message: str
    total_amount: Decimal
    payment: Payment = None
    date_completed: datetime.datetime = None
    date_validated: datetime.datetime = None
    captured_amount: Decimal = Decimal(0)
    payment_provider: str = ''

    def as_json_dict(self):
        return {
            'is_valid': self.is_valid,
            'message': self.message,
            'total_amount': str(self.total_amount),
            'captured_amount': str(self.captured_amount),
            'payment_uid': self.payment.local_ref if self.payment else None,
            'date_completed': str(self.date_completed) if self.date_completed else None,
            'date_validated': str(self.date_validated) if self.date_validated else None,
            'payment_provider': self.payment_provider or ''
        }
