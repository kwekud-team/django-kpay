from kpay.models import Gateway
from kpay.backends import REGISTRY


class GatewayUtils:

    def __init__(self, request):
        self.request = request


def populate_gateways(site):
    user = Gateway.get_system_user()

    for pos, (key, klass) in enumerate(REGISTRY.items()):
        Gateway.objects.get_or_create(
            site=site,
            identifier=key,
            defaults={
                'name': klass.name,
                'description': klass.description or '',
                'sort': pos,
                'created_by': user,
                'modified_by': user
            })
