import logging
from django.contrib.sites.models import Site
from kommons.utils.http import get_client_ip

from kpay.constants import KpayK
from kpay.backends import REGISTRY
from kpay.models import Gateway, Payment
from kpay.contrib.signals.dispatch import kpay_payment_started, kpay_payment_ended, kpay_status_checked
from kpay.contrib.dclasses import PaymentItem, PaymentLink, PaymentProcess


class PayUtils:

    def __init__(self, request, site=None):
        self.request = request
        self.site = site or Site.objects.get_current(request)

    def get_gateway(self, identifier):
        return Gateway.objects.get_or_create(
            site=self.site,
            identifier=identifier,
            defaults={
                'name': identifier,
                'created_by': self.request.user,
                'modified_by': self.request.user
            })[0]

    def get_backend(self, gateway):
        backend_class = REGISTRY.get(gateway.identifier)
        if backend_class:
            return backend_class(self.request, gateway)
        else:
            logging.error("Backend class not found: %s" % gateway.identifier)

    def start_payment(self, gateway_identifier, payment_item: PaymentItem):
        gateway = self.get_gateway(gateway_identifier)
        payment = self._init_payment(gateway, payment_item)
        payment_link = self._get_payment_link(payment)

        kpay_payment_started.send(sender=Payment, request=self.request, payment=payment, payment_link=payment_link)
        return payment_link

    def process_payment_by_webhook(self, gateway_identifier, remote_data) -> PaymentProcess:
        gateway = self.get_gateway(gateway_identifier)
        backend = self.get_backend(gateway)
        payment_process = backend.commit_webhook_payment(gateway, remote_data)

        kpay_payment_ended.send(sender=Payment,
                                request=self.request,
                                payment=payment_process.payment,
                                payment_process=payment_process)
        return payment_process

    def process_payment_by_status(self, payment) -> PaymentProcess:
        backend = self.get_backend(payment.gateway)
        payment_process = backend.commit_status_payment(payment)

        kpay_payment_ended.send(sender=Payment,
                                request=self.request,
                                payment=payment_process.payment,
                                payment_process=payment_process)
        return payment_process

    def get_payment_status(self, source, gateway_identifier, remote_data):
        gateway = self.get_gateway(gateway_identifier)
        backend = self.get_backend(gateway)

        if source == KpayK.FUNC_SOURCE_LOCAL_REF.value:
            payment = self._get_payment(remote_data)
            payment_process = backend.get_local_payment_status(payment)
        else:
            payment_process = backend.get_payment_status(source, remote_data) if backend else None

        kpay_status_checked.send(sender=Payment, request=self.request, payment=payment_process.payment,
                                 payment_process=payment_process)

        return payment_process

    # TODO: Deprecate
    def _get_payment(self, reference):
        return Payment.objects.filter(source_ref=reference, is_active=True).first()

    def _init_payment(self, gateway, payment_item: PaymentItem):
        """
            Payment accepts a unique value for external_ref. It is however possible to make multiple payments for the
            same order. So we append a count string after subsequent payments. Eg: 0000-1111-222, 0000-1111-2222#1
        """
        existing_payment = Payment.objects.filter(
            site=self.site,
            source_ref__startswith=payment_item.source_ref,
            date_completed__isnull=True
        ).first()
        user = self.request.user

        if existing_payment:
            use_existing = True
            # Check status of payment, if it failed previously
            backend = self.get_backend(gateway)
            if backend:
                prev_payment_process = backend.commit_status_payment(existing_payment)
                if prev_payment_process.payment and prev_payment_process.payment.date_completed:
                    use_existing = False

            if use_existing:
                return existing_payment

        previous_qs = Payment.objects.filter(
            site=self.site,
            source_ref__startswith=payment_item.source_ref,
        )
        suffix = f'#{previous_qs.count()}' if previous_qs.exists() else ''

        return Payment.objects.get_or_create(
            site=self.site,
            gateway=gateway,
            source_app_model=payment_item.source_app_model,
            source_ref=f'{payment_item.source_ref}{suffix}',
            date_completed__isnull=True,
            defaults={
                'status': KpayK.STATUS_INITIALIZE.value,
                'description': payment_item.description,
                'total_amount': payment_item.amount,
                'currency': payment_item.currency,
                'ip_address': get_client_ip(self.request),
                'created_by': user,
                'modified_by': user
            })[0]

    def _get_payment_link(self, payment):
        backend = self.get_backend(payment.gateway)
        if backend:
            payment_link_item = backend.get_payment_link(payment)
            if payment_link_item.is_new:
                payment.is_valid = payment_link_item.is_valid
                payment.remote_ref = payment_link_item.payment_remote_ref
                payment.payment_url = payment_link_item.payment_remote_url
                payment.description = payment_link_item.message
                payment.status = KpayK.STATUS_PAYMENT_START.value

                start_json = payment_link_item.response or {}
                start_json.update({
                    'urls': {
                        'callback_url': payment_link_item.payment_callback_url,
                        'redirect_url': payment_link_item.payment_redirect_url
                    }
                })
                payment.response.update({'start': start_json})
                payment.save(update_fields=['is_valid', 'remote_ref', 'status', 'response', 'description',
                                            'payment_url'])
            return payment_link_item

        return PaymentLink(is_valid=False, code='-1', payment_local_ref=payment.local_ref, message='No backend found.',
                           payment=None)

    # def _process_webhook_payment(self, gateway, remote_data):
    #     simulate_type = self.request.GET.get('st', '')
    #
    #     backend = self.get_backend(gateway)
    #     if backend:
    #         payment_process = backend.process_payment_by_webhook(remote_data)
    #         if payment_process.payment:
    #             update_fields = payment_process.update_fields
    #             payment = payment_process.payment
    #             payment.date_validated_webhook = payment_process.date_validated
    #             payment.captured_amount = payment_process.captured_amount
    #             payment.method = payment_process.payment_method
    #             payment.provider_account = payment_process.payment_provider_account
    #             payment.provider = payment_process.payment_provider
    #             payment.provider_ref = payment_process.payment_provider_ref
    #             payment.response.update({'webhook': payment_process.response or {}})
    #             payment.description = payment_process.message
    #             payment.simulate_type = simulate_type
    #
    #             update_fields.append('simulate_type')
    #
    #             if backend.end_payment_func == KpayK.EPF_WEBHOOK.value:
    #                 payment.is_valid = payment_process.is_valid
    #                 payment.date_completed = payment_process.date_completed
    #                 payment.date_validated = payment_process.date_validated
    #                 if payment_process.date_completed:
    #                     payment.status = KpayK.STATUS_PAYMENT_END.value
    #                 else:
    #                     payment.status = KpayK.STATUS_PAYMENT_PENDING.value
    #                 update_fields.extend(['is_valid', 'date_completed', 'date_validated', 'status'])
    #
    #             payment.save(update_fields=update_fields)
    #             return payment_process
    #
    #     return PaymentProcess(process_function=KpayK.EPF_STATUS.value, is_valid=False, message='Payment not found')

    # def _process_status_payment(self, payment):
    #     backend = self.get_backend(payment.gateway)
    #     if backend:
    #         payment_process = backend.process_payment_by_status(payment)
    #         update_fields = payment_process.update_fields
    #         payment = payment_process.payment
    #         payment.date_validated_status = payment_process.date_validated
    #         payment.captured_amount = payment_process.captured_amount
    #         payment.method = payment_process.payment_method
    #         payment.provider = payment_process.payment_provider
    #         payment.provider_account = payment_process.payment_provider_account
    #         payment.provider_ref = payment_process.payment_provider_ref
    #         payment.description = payment_process.message
    #         payment.response.update({'status': payment_process.response or {}})
    #
    #         if backend.end_payment_func == KpayK.EPF_STATUS.value:
    #             payment.is_valid = payment_process.is_valid
    #             payment.date_completed = payment_process.date_completed
    #             payment.date_validated = payment_process.date_validated
    #             if payment_process.date_completed:
    #                 payment.status = KpayK.STATUS_PAYMENT_END.value
    #             else:
    #                 payment.status = KpayK.STATUS_PAYMENT_PENDING.value
    #             update_fields.extend(['is_valid', 'date_completed', 'date_validated', 'status'])
    #
    #         payment.save(update_fields=update_fields)
    #         return payment_process
    #
    #     return PaymentProcess(process_function=KpayK.EPF_STATUS.value, is_valid=False, message='Backend not found')
