import django.dispatch

kpay_status_changed = django.dispatch.Signal()
kpay_payment_started = django.dispatch.Signal()
kpay_payment_ended = django.dispatch.Signal()
kpay_status_checked = django.dispatch.Signal()
