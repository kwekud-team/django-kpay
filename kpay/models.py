import uuid
from django.db import models

from kommons.abstract.models import BaseModel


class Gateway(BaseModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    identifier = models.CharField(max_length=250, db_index=True)
    name = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    sort = models.PositiveIntegerField(default=1000)
    image = models.ImageField(null=True, blank=True, upload_to='kpay/gateway')
    settings = models.JSONField(blank=True, default=dict)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('sort',)
        unique_together = ('site', 'identifier',)


class Payment(BaseModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    is_valid = models.BooleanField(default=False)
    # is_simulated = models.BooleanField(default=False)
    simulate_type = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(max_length=50)
    payment_url = models.URLField(null=True, blank=True)
    # Which content initiated the payment. This is needed to prevent duplicate payments
    source_app_model = models.CharField(max_length=255)
    source_ref = models.CharField(max_length=255)
    remote_ref = models.CharField(max_length=255, blank=True)
    local_ref = models.CharField(max_length=50, unique=True, db_index=True)
    currency = models.CharField(max_length=10)
    total_amount = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    captured_amount = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    ip_address = models.CharField(max_length=50, null=True, blank=True)
    token = models.CharField(max_length=36, blank=True, default='')
    description = models.CharField(max_length=250, null=True, blank=True)
    method = models.CharField(max_length=100, null=True, blank=True, help_text="Eg: Mobile Money, Bank Card")
    provider = models.CharField(max_length=100, null=True, blank=True, help_text="Eg: Visa, Mastercard")
    provider_account = models.CharField(max_length=100, null=True, blank=True, help_text="Eg: Account number, mobile #")
    provider_ref = models.CharField(max_length=100, null=True, blank=True, help_text="Provider Transaction ID")
    request = models.JSONField(blank=True, default=dict)
    response = models.JSONField(blank=True, default=dict)
    date_paid = models.DateTimeField(null=True, blank=True)
    date_completed = models.DateTimeField(null=True, blank=True)
    date_cancelled = models.DateTimeField(null=True, blank=True)
    date_validated = models.DateTimeField(null=True, blank=True)
    date_validated_webhook = models.DateTimeField(null=True, blank=True)
    date_validated_status = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'{self.date_created}: {self.total_amount} - {self.source_ref}'

    class Meta:
        unique_together = ('site', 'gateway', 'source_ref',)

    def save(self, *args, **kwargs):
        self.local_ref = self.local_ref or str(uuid.uuid4()).replace('-', '')
        super().save(*args, **kwargs)

    def get_provider_channel(self):
        if self.provider and self.method:
            return f'{self.method} ({self.provider})'
        elif self.provider:
            return self.provider
        elif self.method:
            return self.method
