from enum import Enum


class KpayK(Enum):
    BACKEND_FREE = 'free'
    BACKEND_STRIPE = 'stripe'
    BACKEND_HUBTEL_BUSINESS = 'hubtel-business'
    BACKEND_HUBTEL_PERSONAL = 'hubtel-personal'

    STATUS_INITIALIZE = 'initialized'
    STATUS_PAYMENT_START = 'payment-start'
    STATUS_PAYMENT_END = 'payment-end'
    STATUS_PAYMENT_PENDING = 'payment-end'
    STATUS_SUCCESS = 'success'
    STATUS_FAILURE = 'failure'
    STATUS_PENDING = 'pending'

    FUNC_SOURCE_LOCAL_REF = 'local_ref'
    FUNC_SOURCE_CALLBACK = 'callback'

    # EPF: End Payment Function
    EPF_WEBHOOK = 'webhook'
    EPF_STATUS = 'status'

    PAY_MESSAGE_MISSING = 'Payment not found'
    PAY_MESSAGE_SUCCESS = 'Payment completed successfully'
    PAY_MESSAGE_FAILURE = 'Payment failed'
    PAY_MESSAGE_PENDING = 'Payment pending'

    # ST: Simulated Type
    ST_SUCCESS = 'success'
    ST_FAILURE = 'failure'
