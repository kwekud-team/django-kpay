from django.urls import path
from kpay import views

app_name = 'kpay'

urlpatterns = [
    path('return/<str:gateway>/', views.PayCallbackView.as_view(), name='return'),
    path('webhook/<str:gateway>/', views.PayWebhookView.as_view(), name='webhook'),
    path('status/<int:payment_pk>/', views.PayStatusView.as_view(), name='status'),
]
