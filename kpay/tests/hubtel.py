import decimal
import json
import httpretty

from kpay.models import Payment
from kpay.constants import KpayK
from kpay.contrib.dclasses import PaymentItem
from kpay.contrib.utils.pay import PayUtils
from kpay.backends.hubtel.core import HubtelBusinessBackend
from kpay.tests.base import BaseTestCase
from kpay.tests.fixtures import hubtel as hubtel_fixt


class HubtelBusinessTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.pay_utils = PayUtils(self.request)
        self._backend_identifier = KpayK.BACKEND_HUBTEL_BUSINESS.value
        self._gateway = self.pay_utils.get_gateway(self._backend_identifier)
        self._app_model = 'app.model'
        self._source_ref = '1'
        self._currency = 'GHS'
        self._payment_item = PaymentItem(
                source_app_model=self._app_model,
                source_ref=self._source_ref,
                currency=self._currency,
                amount=decimal.Decimal(20),
                description='Pay Description',
            )

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_start_payment_unauthorized(self):
        httpretty.register_uri(httpretty.POST, HubtelBusinessBackend.base_url, status=401,
                               body=json.dumps(hubtel_fixt.UNAUTHORIZED))

        pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
        self.assertEqual(pay_link_item.is_valid, False)

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_start_payment_invalid_request(self):
        httpretty.register_uri(httpretty.POST, HubtelBusinessBackend.base_url, status=400,
                               body=json.dumps(hubtel_fixt.INVALID_PAYMENT_REQUEST))

        pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
        self.assertEqual(pay_link_item.is_valid, False)

        payment = Payment.objects.filter(source_app_model=self._app_model, source_ref=self._source_ref).first()
        self.assertIsNotNone(payment)
        self.assertEqual(payment.is_valid, False)
        self.assertEqual(payment.payment_url, '')
        self.assertEqual(payment.remote_ref, '')
        self.assertEqual(payment.status, KpayK.STATUS_PAYMENT_START.value)
        self.assertEqual(payment.total_amount, decimal.Decimal(20))
        self.assertEqual(payment.date_completed, None)

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_start_payment_valid_request(self):
        httpretty.register_uri(httpretty.POST, HubtelBusinessBackend.base_url, status=200,
                               body=json.dumps(hubtel_fixt.VALID_PAYMENT_REQUEST))

        pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
        self.assertEqual(pay_link_item.is_valid, True)

        payment = Payment.objects.filter(source_app_model=self._app_model, source_ref=self._source_ref).first()
        self.assertIsNotNone(payment)
        self.assertEqual(payment.is_valid, True)
        self.assertEqual(payment.status, KpayK.STATUS_PAYMENT_START.value)
        self.assertEqual(payment.payment_url, 'https://pay.hubtel.com/0000-0000/direct')
        self.assertEqual(payment.remote_ref, '0000-0000')
        self.assertEqual(payment.total_amount, decimal.Decimal(20))
        self.assertEqual(payment.date_completed, None)

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_start_payment_duplicate_completed_request(self):
        httpretty.register_uri(httpretty.POST, HubtelBusinessBackend.base_url, status=400,
                               body=json.dumps(hubtel_fixt.DUPLICATE_PAYMENT_REQUEST))

        pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
        self.assertEqual(pay_link_item.is_valid, False)

        payment = Payment.objects.filter(source_app_model=self._app_model, source_ref=self._source_ref).first()
        self.assertIsNotNone(payment)
        self.assertEqual(payment.is_valid, False)
        self.assertEqual(payment.status, KpayK.STATUS_PAYMENT_START.value)
        self.assertEqual(payment.payment_url, '')
        self.assertEqual(payment.remote_ref, '')
        self.assertEqual(payment.description, 'Duplicated client reference. Please try again.')
        self.assertEqual(payment.total_amount, decimal.Decimal(20))
        self.assertEqual(payment.date_completed, None)

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_start_payment_duplicate_not_completed_request(self):
        httpretty.register_uri(httpretty.POST, HubtelBusinessBackend.base_url, status=200,
                               body=json.dumps(hubtel_fixt.VALID_PAYMENT_REQUEST))

        pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
        self.assertEqual(pay_link_item.is_valid, True)

        pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
        self.assertEqual(pay_link_item.is_valid, True)

        payment = Payment.objects.filter(source_app_model=self._app_model, source_ref=self._source_ref).first()
        self.assertIsNotNone(payment)
        self.assertEqual(payment.is_valid, True)
        self.assertEqual(payment.status, KpayK.STATUS_PAYMENT_START.value)
        self.assertEqual(payment.payment_url, 'https://pay.hubtel.com/0000-0000/direct')
        self.assertEqual(payment.remote_ref, '0000-0000')
        self.assertEqual(payment.total_amount, decimal.Decimal(20))
        self.assertEqual(payment.date_completed, None)

    # @httpretty.activate(verbose=True, allow_net_connect=False)
    # def test_start_payment_use_existing_payment_url(self):
    #     httpretty.register_uri(httpretty.POST, HubtelBusinessBackend.base_url, status=200,
    #                            body=json.dumps(hubtel_fixt.VALID_PAYMENT_REQUEST))
    #
    #     pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
    #     self.assertEqual(pay_link_item.is_valid, True)
    #
    #     # Try starting payment again
    #     pay_link_item = PayUtils(self.request).start_payment(self._backend_identifier, self._payment_item)
    #     self.assertEqual(pay_link_item.is_valid, True)
    #
    #     self.assertEqual(pay_link_item)
