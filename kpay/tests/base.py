from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.test.client import RequestFactory


class BaseTestCase(TestCase):

    def setUp(self):
        self.site = self._create_site(domain='parent.testserver')
        self.user = self._create_user('test')
        self.request = RequestFactory().get('/')
        self.request.user = self.user

    def _create_site(self, domain, name=None):
        return Site.objects.create(domain=domain, name=name or domain)

    def _create_user(self, username, password='1234', is_active=True, **kwargs):
        return get_user_model().objects.create_user(username, password=password, is_active=is_active, **kwargs)
