
UNAUTHORIZED = {}

INVALID_PAYMENT_REQUEST = {
    "responseCode": "4000",
    "status": "Error",
    "data": [
        {
            "field": "CallbackUrl",
            "errorMessage": "Callback URL is invalid."
        },
    ]
}

VALID_PAYMENT_REQUEST = {
    "data": {
        "checkoutDirectUrl": "https://pay.hubtel.com/0000-0000/direct",
        "checkoutId": "0000-0000",
        "checkoutUrl": "https://pay.hubtel.com/0000-0000",
        "clientReference": "5c7b5d67-edfb-4c8e-bcd7-48be7bd1c952",
        "message": ""},
    "responseCode": "0000",
    "status": 'Success'
}

DUPLICATE_PAYMENT_REQUEST = {
    "responseCode": "4000",
    "status": "Error",
    "data": [
        {
            "field": "ClientReference",
            "errorMessage": "Duplicated client reference. Please try again."
        }
    ]
}

PAYMENT_WEBHOOK_SUCCESS = {
    "ResponseCode": "0000",
    "Status": "Success",
    "Data": {
        "CheckoutId": "P000-0001",
        "SalesInvoiceId": "R000-0001",
        "ClientReference": "C000-0001",
        "Status": "Success",
        "Amount": 0.11,
        "CustomerPhoneNumber": "23320123405",
        "PaymentDetails": {
            "MobileMoneyNumber": "23320123405",
            "PaymentType": "hubtel",
            "Channel": None
        },
        "Description": "Success Withdrawal"
    }
}

PAYMENT_WEBHOOK_FAILURE = {
    "ResponseCode": "-1",
    "Status": "Failure",
    "Data": {
        "CheckoutId": "P000-0002",
        "SalesInvoiceId": "R000-0002",
        "ClientReference": "C000-0002",
        "Status": "Success",
        "Amount": 0.11,
        "CustomerPhoneNumber": "23320123405",
        "PaymentDetails": {
            "MobileMoneyNumber": "23320123405",
            "PaymentType": "hubtel",
            "Channel": None
        },
        "Description": "Failure"
    }
}

PAYMENT_WEBHOOK_LOW_BALANCE = {
    "Status": "Failed",
    "ResponseCode": "2001",
    "Data": {
        "Amount": 0.11,
        "Status": "Failed",
        "CheckoutId": "P000-0003",
        "Description": "Low balance",
        "PaymentDetails": {
            "Channel": None,
            "PaymentType": "hubtel",
            "MobileMoneyNumber": "23320123405"
        },
        "SalesInvoiceId": "R000-0003",
        "ClientReference": "C000-0003",
        "CustomerPhoneNumber": "23320123405"
    },
}
