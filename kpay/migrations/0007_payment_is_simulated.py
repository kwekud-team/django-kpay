# Generated by Django 4.0.1 on 2022-10-10 23:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kpay', '0006_payment_provider_account'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='is_simulated',
            field=models.BooleanField(default=False),
        ),
    ]
