import setuptools

long_description = ''
try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    pass

setuptools.setup(
     name='kpay',
     version='0.1.18',
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="Payment integrations",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/django-kpay",
     packages=setuptools.find_packages(),
     include_package_data=True,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     install_requires=[
         'stripe',
         'httpretty'
     ]
)
